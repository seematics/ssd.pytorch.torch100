from torch import nn

from collections import OrderedDict

from torchvision.models import vgg

base = {
    '300': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'C', 512, 512, 512, 'M', 512, 512, 512],
    '512': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'C', 512, 512, 512, 'M', 512, 512, 512],
    'source1': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'C', 512, 512, 512],
    'source2': ['M', 512, 512, 512],
}


class CleanSSD(nn.Module):

    def __init__(self, batch_norm=False, output_dim=1024, in_channels=3):
        super(CleanSSD, self).__init__()

        input_dimension = in_channels
        self.source1, major_conv, minor_conv = make_layers(base['source1'], in_chans=input_dimension, batch_norm=batch_norm)

        input_dimension = self.source1[-2].out_channels
        self.rest_of_vgg, _, _ = make_layers(base['source2'], in_chans=input_dimension, batch_norm=batch_norm, major_conv=major_conv,
                                       minor_conv=minor_conv)

        input_dimension = self.rest_of_vgg[-2].out_channels
        self.source2 = nn.Sequential(OrderedDict([('pool5', nn.MaxPool2d(kernel_size=3, stride=1, padding=1)),
                                                  ('conv6', nn.Conv2d(input_dimension, 1024, kernel_size=3, padding=6, dilation=6)),
                                                  ('conv6_relu', nn.ReLU(inplace=True)),
                                                  ('conv7', nn.Conv2d(512, 1024, kernel_size=3, padding=6, dilation=6)),
                                                  ('conv7_relu', nn.ReLU(inplace=True)),
                                                  ]))

        self.extra_sources = []
        input_dimension = self.source2[-2].out_channels
        for _ in range(4):
            self.extra_sources.append(ExtraLayer(input_dim=input_dimension, layer_dim=256, out_dim=512, last_kernel=3))
            nn.ModuleList()
            input_dimension = self.extra_sources[-1].conv2.out_channels
        self.extra_sources.append(ExtraLayer(input_dim=input_dimension, layer_dim=256, out_dim=512, last_kernel=4))

        self.extra_sources = nn.ModuleList(self.extra_sources)

    def forward(self, images):
        sources = []
        x = self.source1(images)
        sources.append(x)
        x = self.rest_of_vgg(x)
        x = self.source2(x)
        sources.append(x)

        for src in self.extra_sources:
            x = src(x)
            sources.append(x)

        return x


def make_layers(cfg, in_chans=3, batch_norm=False, major_conv=1, minor_conv=1):
    layers = []
    in_channels = in_chans

    for v in cfg:
        if v == 'M':
            layers += [('pool%d' % major_conv, nn.MaxPool2d(kernel_size=2, stride=2))]
            major_conv += 1
            minor_conv = 1
        elif v == 'C':
            layers += [('pool%d' % major_conv, nn.MaxPool2d(kernel_size=2, stride=2, ceil_mode=True))]
            major_conv += 1
            minor_conv = 1
        else:
            conv_str = 'conv%d_%d' % (major_conv, minor_conv)
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [(conv_str, conv2d), ('%s_bn' % conv_str, nn.BatchNorm2d(v)), ('%s_relu' % conv_str, nn.ReLU(inplace=True))]
            else:
                layers += [(conv_str, conv2d), ('%s_relu' % conv_str, nn.ReLU(inplace=True))]
            in_channels = v
            minor_conv += 1

    return nn.Sequential(OrderedDict(layers)), major_conv, minor_conv


class ExtraLayer(nn.Module):

    def __init__(self, input_dim, layer_dim, out_dim, last_kernel):
        super(ExtraLayer, self).__init__()

        self.conv1 = nn.Conv2d(input_dim, layer_dim, kernel_size=1)
        self.conv2 = nn.Conv2d(self.conv1.out_channels, out_dim, kernel_size=last_kernel, stride=2, padding=1)

        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv1(x)
        x = self.relu(x)
        x = self.conv2(x)
        x = self.relu(x)
        return x



def add_extras_512(cfg, i, hmap_channels=0):
    # Extra layers added to VGG for feature scaling
    layers = []
    add_hmap_chans = 1
    in_channels = i
    flag = False
    for k, v in enumerate(cfg[:-1]):
        if in_channels != 'S':
            if v == 'S':
                layers += [nn.Conv2d(in_channels, cfg[k + 1], kernel_size=(1, 3)[flag], stride=2, padding=1)]
            else:
                layers += [nn.Conv2d(in_channels + add_hmap_chans*hmap_channels, v, kernel_size=(1, 3)[flag])]
            flag = not flag
            add_hmap_chans = 0
        else:
            add_hmap_chans = 1
        in_channels = v

    layers += [nn.Conv2d(cfg[-2], cfg[-1], kernel_size=4, padding=1)]
    return layers



if __name__ == '__main__':
    net = CleanSSD()
    print(net.state_dict().keys())
