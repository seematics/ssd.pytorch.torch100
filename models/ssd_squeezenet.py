import torch.nn as nn
from torchvision.models.squeezenet import Fire
from models.ssd_vgg import SSD, extras, mbox, add_extras, add_extras_512


# This function is derived from torchvision SqueezeNet
# https://github.com/pytorch/vision/blob/master/torchvision/models/squeezenet.py
def squeezenet(version=1.0):
    layers = []

    if version not in [1.0, 1.1]:
        raise ValueError("Unsupported SqueezeNet version {version}:"
                         "1.0 or 1.1 expected".format(version=version))
    if version == 1.0:
        layers += [nn.Conv2d(3, 96, kernel_size=7, stride=2)]
        layers += [nn.ReLU(inplace=True)]
        layers += [nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)]
        layers += [Fire(96, 16, 64, 64)]
        layers += [Fire(128, 16, 64, 64)]
        layers += [Fire(128, 32, 128, 128)]
        layers += [nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)]
        layers += [Fire(256, 32, 128, 128)]
        layers += [Fire(256, 48, 192, 192)]
        layers += [Fire(384, 48, 192, 192)]
        layers += [Fire(384, 64, 256, 256)]
        layers += [nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)]
        layers += [Fire(512, 64, 256, 256)]
    else:
        layers += [nn.Conv2d(3, 64, kernel_size=3, stride=2)]
        layers += [nn.ReLU(inplace=True)]
        layers += [nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)]
        layers += [Fire(64, 16, 64, 64)]
        layers += [Fire(128, 16, 64, 64)]
        layers += [nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)]
        layers += [Fire(128, 32, 128, 128)]
        layers += [Fire(256, 32, 128, 128)]
        layers += [nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)]
        layers += [Fire(256, 48, 192, 192)]
        layers += [Fire(384, 48, 192, 192)]
        layers += [Fire(384, 64, 256, 256)]
        layers += [Fire(512, 64, 256, 256)]
    return layers


def multibox_squeezenet(features, extra_layers, cfg, num_classes, features_source):
    loc_layers = []
    conf_layers = []
    for k, v in enumerate(features_source):
        loc_layers += [nn.Conv2d(features[v].expand1x1.out_channels + features[v].expand3x3.out_channels,
                                 cfg[k] * 4, kernel_size=3, padding=1)]
        conf_layers += [nn.Conv2d(features[v].expand1x1.out_channels + features[v].expand3x3.out_channels,
                                  cfg[k] * num_classes, kernel_size=3, padding=1)]
    for k, v in enumerate(extra_layers[1::2], 2):
        loc_layers += [nn.Conv2d(v.out_channels, cfg[k]
                                 * 4, kernel_size=3, padding=1)]
        conf_layers += [nn.Conv2d(v.out_channels, cfg[k]
                                  * num_classes, kernel_size=3, padding=1)]
    return features, extra_layers, (loc_layers, conf_layers)


def build_ssd_with_squeezenet(cfg, phase, variant=300, size=(300, 300), num_classes=21, extras_in_channels=512,
                              version=1.0, dropout=0.1):
    if phase != "test" and phase != "train":
        print("ERROR: Phase: " + phase + " not recognized")
        return
    if version == 1.0:
        features_source = [10, -1]
        conv4_3 = 11
        l2_n_channels = 512
    elif version == 1.1:
        features_source = [7, -1]
        conv4_3 = 8
        l2_n_channels = 256
    else:
        raise ValueError("version number: " + str(version) + " not recognized. We support only '1.0' or '1.1'")

    add_extras_func = {300: add_extras, 512: add_extras_512}[variant]
    base_, extras_, head_ = multibox_squeezenet(squeezenet(version=version),
                                                add_extras_func(extras[str(variant)], extras_in_channels),
                                                mbox[str(variant)], num_classes, features_source=features_source)
    return SSD(phase, size, base_, extras_, head_, num_classes=num_classes, cfg=cfg, conv4_3=conv4_3, dropout=dropout,
               l2_n_channels=l2_n_channels)
