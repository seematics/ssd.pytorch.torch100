import torch
import numpy as np
import cv2
import time
import yaml

from argparse import ArgumentParser
from copy import copy
from functools import partial
from logging import ERROR

from pathlib2 import Path
from torch.autograd import Variable

from common.metrics import BoxesHistory, average_precision_from_curve
from common.utils import ssd_output_to_allegro_format, setup_pytorch03, ImageSizeTuple, build_ssd, make_deterministic
from common.visualization import DrawBoxesAndLabels
from common.batchers import make_ssd_batch

from allegroai import DataView, Task, InputModel
from allegroai.utilities.datapipes import ScanningDataPipe
from allegroai.dataview import IterationOrder, FilterByRoi
from allegroai.utilities.numerical import nms_by_score
from allegroai_api.services import tasks
from allegroai.utilities.plotly import SeriesInfo


DEFAULT_OVERLAP_RATIO = 0.2
MAX_POSSIBLE_BATCH_SIZE = 1
assert MAX_POSSIBLE_BATCH_SIZE == 1, "sorry, only batch size of 1 is implemented"

# Please see __main__ below.
# =================   GLOBAL NAMES - meant to be overridden by ui, help reuse task id  ==============================
TASK_NAME = 'Test SSD [scan mode] example'
PROJECT_NAME = 'pytorch ssd'

# epsilon for numerical methods:
EPS = np.finfo(float).eps


def get_parser(input_parser=None):
    parser = input_parser or ArgumentParser(
        description='Single Shot MultiBox Detector Test model With Pytorch')

    # (0) Basic config
    parser.add_argument('--test-size', default=10000, type=int, help='Number of iterations for test')
    parser.add_argument('--report-iterations', default=100, type=int, help='Report Iterations')
    parser.add_argument('--report-images-every-n-reports', default=3, type=int, help='Images report frequency')
    parser.add_argument('--num-workers', default=32, type=int, help='Number of workers used for parallel data loading')
    parser.add_argument('--upload-destination', default='s3://allegro-examples', type=str,
                        help='Destination to upload debug images and models')
    parser.add_argument('--feature-extraction-type', default='vgg16', type=str, help='Feature extraction network')

    # (1) Override model design if needed:
    parser.add_argument('--override-conf-thresh', default=0, type=int, help='override specified conf thresh')
    parser.add_argument('--override-nms-thresh', default=0, type=int, help='override specified nms thresh')
    parser.add_argument('--override-input-w', default=0, type=int, help='override specified w')
    parser.add_argument('--override-input-h', default=0, type=int, help='override specified h')

    # (2) mapping and dataview control
    parser.add_argument('--hard-mapping-for-test', default=1, type=int, help='Map occluded objects to hard in test')

    # (3) pytorch specific
    parser.add_argument('--cuda', default=1, type=int, help='Use CUDA to train model')

    # source id
    parser.add_argument('--source-id', type=str, default=None,
                        help='In case of several sources per frame, only choose this source id to test on')
    return parser


def test_model(net, config_params, args, test_dataview, test_mapping, test_draw_func, logger, at_iteration=0,
               cuda_on=True):
    # hardcoded parameters:
    precision_recall_conf_list = [0.001, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 0.99, 0.999]
    min_precision_for_integrated_metric = 0.7

    target_image_size = ImageSizeTuple(w=config_params['min_dim_w'], h=config_params['min_dim_h'])

    if args.test_size <= 0 or not test_dataview:
        logger.console('Skipping test, no frames to test')
        return

    # Set net to eval mode for testing
    net.eval()

    logger.console('%s' % str(80 * '-'))
    t_test = time.time()
    iou = [i for i in config_params['test_detection_iou_thresh']]

    batcher = partial(make_ssd_batch, target_image_size_wh=target_image_size, source_id=args.source_id)

    iterator = test_dataview.get_iterator()

    if args.source_id:
        source_id = [args.source_id]
    else:
        source_id = None
    test_pipe = ScanningDataPipe(iterator=iterator,
                                 frame_cls_kwargs=dict(crop_width=target_image_size.w,
                                                       crop_height=target_image_size.h,
                                                       overlap_percent=DEFAULT_OVERLAP_RATIO,
                                                       default_source_ids_to_load=source_id))

    test_pipe_iterator = test_pipe.get_iterator()
    logger.console('Testing currently loaded weights...')

    box_history = BoxesHistory()
    # TODO: refactor
    last_meta = None
    scanned_img = None
    report_boxes = report_image = report_scores = report_meta = None
    all_batch_boxes = all_batch_scores = all_ground_truth = all_hard_boxes = None
    report_ground_truth = report_ground_truth_after_nms = report_hard_boxes = report_hard_boxes_after_nms = None

    pre_batch_collect = []
    report_iter = 0
    for test_iter, image_frame in enumerate(test_pipe_iterator):
        if report_iter > args.test_size:
            break

        upper_left_xy = image_frame._upper_left_xy
        meta_frame = image_frame.get_meta_data()
        width = meta_frame.width
        height = meta_frame.height
        if upper_left_xy == [0, 0]:
            if test_iter > 0:
                report_meta = copy(last_meta)
                report_image = scanned_img.copy() if scanned_img is not None else None
                report_boxes = all_batch_boxes.copy() if all_batch_boxes is not None else None
                report_scores = all_batch_scores.copy() if all_batch_scores is not None else None
                report_ground_truth = all_ground_truth.copy() if all_ground_truth is not None else None
                report_hard_boxes = all_hard_boxes.copy() if all_hard_boxes is not None else None
            scanned_img = np.zeros(shape=(height, width, 3)).astype('uint8')
            all_batch_boxes = None
            all_batch_scores = None
            all_ground_truth = None
            all_hard_boxes = None

        last_meta = meta_frame
        scanned_img[
            upper_left_xy[1]:upper_left_xy[1] + test_pipe._frame_cls_kwargs['crop_height'],
            upper_left_xy[0]:upper_left_xy[0] + test_pipe._frame_cls_kwargs['crop_width'], :] = image_frame.get_data()

        pre_batch_collect.append(image_frame)
        if len(pre_batch_collect) != MAX_POSSIBLE_BATCH_SIZE:
            continue

        # collect the frames into a batch (i.e. a list of tuples)
        image_batch = pre_batch_collect
        pre_batch_collect = []
        # use the batch and zero it at the end

        # test batch
        batch = batcher(image_batch)
        batch_input = [torch.from_numpy(inp) for inp in batch.input]
        batch_input = [Variable(inp.cuda()) if cuda_on else Variable(inp) for inp in batch_input]

        # forward
        detections = net(*batch_input)
        detections = detections.data.cpu().numpy()
        curr_batch_boxes, curr_batch_scores = \
            ssd_output_to_allegro_format(numpy_ssd_output=detections,
                                         conf_thresh=min(precision_recall_conf_list),
                                         img_width=target_image_size.w,
                                         img_height=target_image_size.h)

        # Make sure we have hard rois
        curr_ground_truth = tuple(bb for bb in (batch.ground_truth, batch.hard_ground_truth) if bb.size)
        curr_hard_boxes = batch.hard_ground_truth

        if curr_ground_truth:
            curr_ground_truth = np.vstack(curr_ground_truth)
        else:
            curr_ground_truth = None

        if len(curr_batch_boxes):
            shifted_boxes = curr_batch_boxes.copy()
            shifted_boxes[:, 1:5] += upper_left_xy * 2
            if all_batch_boxes is None:
                all_batch_boxes = shifted_boxes
                all_batch_scores = curr_batch_scores
            else:
                all_batch_boxes = np.vstack((all_batch_boxes, shifted_boxes))
                all_batch_scores = np.hstack((all_batch_scores, curr_batch_scores))

        if curr_ground_truth is not None:
            shifted_boxes = curr_ground_truth.copy()
            shifted_boxes[:, 1:5] += upper_left_xy * 2
            if all_ground_truth is None:
                all_ground_truth = shifted_boxes
            else:
                all_ground_truth = np.vstack((all_ground_truth, shifted_boxes))
                curr_ground_truth = None

        if len(curr_hard_boxes):
            hard_shifted_boxes = curr_hard_boxes.copy()
            hard_shifted_boxes[:, 1:5] += upper_left_xy * 2
            if all_hard_boxes is None:
                all_hard_boxes = hard_shifted_boxes
            else:
                all_hard_boxes = np.vstack((all_hard_boxes, hard_shifted_boxes))
                curr_hard_boxes = None

        if upper_left_xy == [0, 0] and test_iter > 0:
            if report_ground_truth is not None:
                gt_indexes = nms_by_score(report_ground_truth[:, 1:5], np.ones(len(report_ground_truth)),
                                          overlapThresh=0.5, type='by_area')
                report_ground_truth_after_nms = np.atleast_2d(report_ground_truth[gt_indexes])
            else:
                report_ground_truth_after_nms = None

            if report_hard_boxes is not None:
                hard_indexes = nms_by_score(report_hard_boxes[:, 1:5], np.ones(len(report_hard_boxes)),
                                            overlapThresh=0.5, type='by_area')
                report_hard_boxes_after_nms = [np.atleast_2d(report_hard_boxes[hard_indexes])[:, 1:5]]
            else:
                report_hard_boxes_after_nms = None

            if report_boxes is not None:
                pred_indexes = nms_by_score(report_boxes[:, 1:5], report_scores, overlapThresh=0.5, type='by_score')
                report_boxes_after_nms = np.atleast_2d(report_boxes[pred_indexes])
                report_scores_after_nms = report_scores[pred_indexes]
            else:
                report_boxes_after_nms = None
                report_scores_after_nms = None

            if report_ground_truth_after_nms is not None:
                box_history.save_frame_data(pred_boxes=report_boxes_after_nms, pred_scores=report_scores_after_nms,
                                            gt_boxes=report_ground_truth_after_nms)
            report_iter += 1
            if report_iter and report_iter % args.report_iterations == 0:
                msg = 'Test iteration %-6d' % report_iter
                logger.console(msg)
            if report_iter and report_iter % (args.report_iterations * args.report_images_every_n_reports) == 0:
                t_images = time.time()
                data_name = ['Images']

                if len(data_name) < len(batch.input):
                    logger.console('Too many inputs, not designed for such amount, using default names', level=ERROR)
                    data_name.extend(['']*len(batch.input))

                for stage_id, np_images in enumerate([np.array([report_image])]):
                    draws = test_draw_func(images=np_images,
                                           pred_scores=report_scores_after_nms,
                                           pred_boxes=report_boxes_after_nms,
                                           gt_boxes=report_ground_truth_after_nms,
                                           gt_boxes_labels=None,
                                           gt_boxes_hard=report_hard_boxes_after_nms,
                                           gt_boxes_hard_labels=None)

                    draws = [cv2.putText(im, 'TEST', (10, 30), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255))
                             for im in draws]

                    for idx, img in enumerate(draws):
                        logger.report_image_and_upload(title='Test - %s' % data_name[stage_id],
                                                       series='img_%d - %s' % (report_iter, Path(report_meta.src).name),
                                                       iteration=at_iteration, matrix=img.astype(np.uint8))

                t_images = time.time() - t_images
                logger.console('Iter %d Time to create test images: %.4f (sec)' % (report_iter, t_images))
                logger.flush()

    # Report results
    id_to_label = {v: k for k, v in test_mapping.items() if v > 0}
    logger.console('Calculating precision and creating report...')

    for iou_thresh in iou:
        pr_curves_per_label = box_history.get_precision_recall_curve(
            iou_threshold=iou_thresh,
            conf_thresholds=precision_recall_conf_list,
            nms_threshold=config_params.get('nms_thres_for_metrics', 0.65))

        all_ap = []
        all_neap = []
        prec_recall_series = []
        conf_fscore_series = []
        simple_title = '{} | IOU=%3.2f' % iou_thresh
        for label_id in pr_curves_per_label:
            pr_curve = pr_curves_per_label[label_id]
            label_str = id_to_label.get(label_id, 'Label %d' % label_id)
            ap = average_precision_from_curve(pr_curve)
            ap_with_min_prec = average_precision_from_curve(pr_curve, min_precision=min_precision_for_integrated_metric)
            min_prec = min_precision_for_integrated_metric
            prec_recall_graph = [(recall, precision) for _, precision, recall in pr_curve.raw_curve]
            conf_fscore_graph = [(conf, ((2 * recall * precision) / (EPS + recall + precision)))
                                 for conf, precision, recall in pr_curve.raw_curve]
            top_fscore = max([x[1] for x in conf_fscore_graph])
            logger.console('AP=%.2f AP_with_min_prec=%.2f' % (ap, ap_with_min_prec))
            prec_recall_series.append(SeriesInfo(
                name=label_str,
                data=prec_recall_graph,
                labels=['Recall=%3.2f, Precision=%3.2f' % (x[0], x[1]) for x in prec_recall_graph]
            ))
            conf_fscore_series.append(SeriesInfo(
                name=label_str,
                data=conf_fscore_graph,
                labels=['conf_thr=%3.2f' % x[0] for x in conf_fscore_graph]
            ))
            logger.report_scalar(
                title=simple_title.format('Top F1-score'),
                series='%s' % label_str,
                iteration=at_iteration,
                value=top_fscore)
            logger.report_scalar(
                title=simple_title.format('normalized Effective AP_%3.2f' % min_prec),
                series='%s' % label_str,
                iteration=at_iteration,
                value=ap_with_min_prec)
            all_neap.append(ap_with_min_prec)
            logger.report_scalar(
                title=simple_title.format('Average Precision'),
                series='%s' % label_str,
                iteration=at_iteration,
                value=ap)
            all_ap.append(ap)

            logger.console(simple_title.format('Precision-Recall'))
            logger.console(prec_recall_graph)
            logger.console('-------------------------------------------------')
            logger.console(simple_title.format('Conf-F1-score'))
            logger.console(conf_fscore_graph)
            logger.console('-------------------------------------------------')

        logger.report_line_plot(
            title="Precision-Recall @IOU:%s" % iou_thresh,
            series=prec_recall_series,
            iteration=at_iteration,
            xaxis='Recall',
            yaxis='Precision')

        logger.report_line_plot(
            title="Conf-F1-score @IOU:%s" % iou_thresh,
            series=conf_fscore_series,
            iteration=at_iteration,
            xaxis='Conf. Threshold',
            yaxis='F1-Score')

        logger.report_scalar(
            title=simple_title.format('normalized Effective AP_%3.2f' % min_precision_for_integrated_metric),
            series='mnEAP_%3.2f' % min_precision_for_integrated_metric,
            iteration=at_iteration,
            value=np.mean(all_neap))
        logger.report_scalar(
            title=simple_title.format('Average Precision'),
            series='mAP',
            iteration=at_iteration,
            value=np.mean(all_ap))

        logger.flush()

    logger.console('Finished testing snapshot!')
    logger.console('%s' % str(80 * '-'))
    msg = 'Iteration %-6d : test time %.3f (sec)' % (at_iteration, time.time() - t_test)
    logger.console(msg)
    logger.flush()


if __name__ == '__main__':
    ###################################
    # Create task in allegro's system #
    ###################################
    connect_to_task_parser = get_parser()
    task = Task.current_task(default_project_name=PROJECT_NAME, default_task_name=TASK_NAME,
                             default_task_type=tasks.TaskTypeEnum.testing)
    seed = task.get_random_seed()
    make_deterministic(seed)  # setup random seed from task for reproducibility

    #################################
    # Connect arguments to the task #
    #################################
    task.connect(connect_to_task_parser)
    args = connect_to_task_parser.parse_args()
    if args.source_id == '':
        args.source_id = None

    logger = task.get_logger()
    logger.set_default_upload_destination(uri=args.upload_destination)
    logger.console('Running arguments: %s' % str(args))

    # (1) Import Model Weights =========================================================================================
    INPUT_MODEL_URL = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_Person_Detector.pth'
    INPUT_MODEL_NAME = 'Pytorch SSD person detector'
    input_model = InputModel.import_model(weights_url=INPUT_MODEL_URL, name=INPUT_MODEL_NAME,
                                          design=None, label_enumeration=None)
    input_model.publish()
    task.connect(input_model)

    # (2) Setup the output Model labels (mapping to integer IDs) =======================================================
    default_labels = {'hard': -2, 'ignore': -1, 'background': 0,  'Car': 1, 'Person': 2, 'Van': 3,
                      'Cyclist': 4, 'Truck': 5, 'Tram': 6}
    input_model_labels = input_model.labels
    if not input_model_labels or all([value <= 0 for value in input_model_labels.values()]):
        logger.console('No input model labels, or no positive ids : %s , using default labels for this script'
                       % input_model_labels, level=ERROR)
        input_model_labels = default_labels

    current_task_labels = input_model_labels
    positive_model_labels = [v for _, v in current_task_labels.items() if v > 0]
    model_num_positive_classes = len(positive_model_labels)
    if not model_num_positive_classes:
        raise ValueError('No positive label id defined for the model? currently : %s' % str(current_task_labels))
    elif model_num_positive_classes != max(positive_model_labels):
        raise ValueError('Not all model labels have a defined id, cannot run task. num id >0 : '
                         '%d while largest id is %d' % (model_num_positive_classes, max(positive_model_labels)))

    # (3) Dataviews ====================================================================================================
    test_dataview = DataView(iteration_order=IterationOrder.sequential, iteration_infinite=False)
    test_dataview.add_query(dataset_name='KITTI-2D-OBJECT',
                            version_name='training',
                            filter_by_roi=FilterByRoi.disabled)
    task.connect(test_dataview)

    # == (4) Mapping ===========================================================================================
    label_to_id_mapping_for_test = copy(current_task_labels)
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='Person_sitting', to_label='person')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='Pedestrian', to_label='person')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='Cyclist', to_label='person')
    if args.hard_mapping_for_test:
        # make sure we have 'hard' id
        label_to_id_mapping_for_test['hard'] = -2
        # map all difficult persons to hard
        test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                       from_labels=['largely_occluded', 'Person_sitting'], to_label='hard')
        test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                       from_labels=['largely_occluded', 'Pedestrian'], to_label='hard')
        test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                       from_labels=['largely_occluded', 'Cyclist'], to_label='hard')

    test_dataview.set_labels(label_to_id_mapping_for_test)

    # == (4.3) Apply mapping to debug images ===========================================================================
    id_to_label_mapping_for_test = {v: k for k, v in label_to_id_mapping_for_test.items()}

    # (5) Build Model (pytorch specific from here on) ==================================================================
    # As above - changes made in the design at the web application will override the following:
    config_params = yaml.load(task.get_model_design())  # Enable changing network design in the web app
    if args.override_conf_thresh > 0:
        previous_conf = config_params.pop('test_detection_conf_thresh', 'Not Specified')
        config_params['test_detection_conf_thresh'] = float(args.override_conf_thresh)
        logger.console('Override: confidence thresh for metrics is now %d instead of %s' %
                       (config_params['test_detection_conf_thresh'], str(previous_conf)))
    if args.override_nms_thresh > 0:
        previous_nms = config_params.pop('nms_thresh_for_metrics', 'Not Specified')
        config_params['nms_thresh_for_metrics'] = float(args.override_nms_thresh)
        logger.console('Override: NMS thresh for metrics is now %d instead of %s' %
                       (config_params['nms_thresh_for_metrics'], str(previous_nms)))
    if args.override_input_w > 0:
        previous_w = config_params['min_dim_w']
        config_params['min_dim_w'] = args.override_input_w
        logger.console('Override: input width is now %d instead of %d' %
                       (config_params['min_dim_w'], previous_w))
    if args.override_input_h > 0:
        previous_h = config_params['min_dim_h']
        config_params['min_dim_h'] = args.override_input_h
        logger.console('Override: input width is now %d instead of %d' %
                       (config_params['min_dim_h'], previous_h))

    target_image_size = ImageSizeTuple(w=config_params['min_dim_w'], h=config_params['min_dim_h'])
    score_thresh = config_params.get('test_detection_conf_thresh', 0.5)
    draw_boxes_with_mapping_for_test = DrawBoxesAndLabels(labels_mapping=id_to_label_mapping_for_test, logger=logger,
                                                          score_threshold=score_thresh)

    # complain but adjust num-classes if there are more model labels than classes
    design_num_classes = config_params.setdefault('num_classes', 1 + model_num_positive_classes)
    if design_num_classes != 1 + model_num_positive_classes:
        logger.console('Model design contained different number of classes than expected', ERROR)
        raise IOError('model and design mismatch. expected %d classes, task defines %d' %
                      (design_num_classes, 1 + model_num_positive_classes))

    cuda_on = setup_pytorch03(args=args, logger=logger, suppress_warnings=True)

    # we do not use the internal detector so phase stays as train
    variant = 512 if max(target_image_size) >= 400 else 300
    ssd_net, freeze_n = build_ssd(args, config_params, variant, target_image_size)

    # wrap to enable multiple gpus
    net = torch.nn.DataParallel(ssd_net) if cuda_on else ssd_net

    # (6) (Finally) Load Weights and test ==============================================================================

    batcher_func = make_ssd_batch
    weights_file = input_model.get_weights()
    ssd_net.forgiving_load(weights_file, init_func_on_skip=None, logger=logger)

    # test_model1()
    test_model(net, config_params, args, test_dataview,
               test_mapping=label_to_id_mapping_for_test, test_draw_func=draw_boxes_with_mapping_for_test,
               logger=logger, at_iteration=0, cuda_on=cuda_on)
