from logging import ERROR

import cv2
import datetime
from argparse import ArgumentParser

import yaml
import numpy as np
from copy import copy
import torch
from torch.autograd import Variable

from allegroai import DatasetVersion, DataView, Task, InputModel
from allegroai.dataview import FilterByRoi
from allegroai.dataview import IterationOrder
from allegroai.debugging.timer import Timer
from allegroai.utilities.datapipes import ScanningDataPipe
from allegroai.utilities.numerical import nms_by_score
from allegroai_api.services import tasks

from common.utils import ssd_output_to_allegro_format, setup_pytorch03, ImageSizeTuple, build_net, make_deterministic

# Please see __main__ below.
# =================  GLOBAL NAMES - meant to be overridden by ui, help reuse task id  ==============================
TASK_NAME = 'Create annotated version using SSD [scan mode]'
PROJECT_NAME = 'pytorch ssd'
DEFAULT_OVERLAP_RATIO = 0.2
MAX_POSSIBLE_BATCH_SIZE = 1


# convert ROI to polygon (as supported by the frame)
def box_to_poly(box):
    top_left = [box[0], box[1]]
    top_right = [box[2], box[1]]
    bottom_right = [box[2], box[3]]
    bottom_left = [box[0], box[3]]
    poly = np.hstack((top_left, top_right, bottom_right, bottom_left)).tolist()
    return poly


# uses model design values, for which there is an override using execution parameters
def ssd_inference_on_image(single_image):
    single_image = torch.from_numpy(single_image)
    in_image = Variable(single_image.cuda())

    # forward
    detections = net.data_parallel(in_image)
    detections = detections.data.cpu().numpy()
    all_image_boxes, all_image_scores = \
        ssd_output_to_allegro_format(numpy_ssd_output=detections,
                                     conf_thresh=config_params['test_detection_conf_thresh'],
                                     img_width=config_params['min_dim_w'],
                                     img_height=config_params['min_dim_h'])
    return all_image_boxes, all_image_scores


def prepare_report_rois(report_boxes, report_scores):
    if report_boxes is not None:
        pred_indexes = nms_by_score(report_boxes[:, 1:5], report_scores, overlapThresh=0.5,
                                    type='by_score')
        report_boxes_after_nms = np.atleast_2d(report_boxes[pred_indexes])
        report_scores_after_nms = report_scores[pred_indexes]
    else:
        report_boxes_after_nms = None
        report_scores_after_nms = None

    img_rois = []
    if report_boxes_after_nms is not None:
        for n, bbox in enumerate(report_boxes_after_nms):
            # note that this works since we have a single image in the batch
            box = bbox[1:5]
            label = [id_to_model_label.get(bbox[5])]
            score = report_scores_after_nms[n]
            img_rois.append(DataView.FrameRoi(
                sources=[source],
                label=label,
                poly=box_to_poly(list(box)),
                confidence=score))
    return img_rois, report_boxes_after_nms


frame_process_timer = Timer()
frame_get_image_timer = Timer()
frame_inference_timer = Timer()


if __name__ == '__main__':
    parser = ArgumentParser(description='Create annotated dataset version')
    parser.add_argument('--new-version-name', help='Name for the generated version')
    parser.add_argument('--feature-extraction-type', default='vgg16', type=str, help='Feature extraction network')
    parser.add_argument('--debug-image-freq', default=100, type=int, help='upload debug images every n frames')
    parser.add_argument('--create-at-conf-thresh', default=0, type=float, help='threshold used for inference')
    parser.add_argument('--override-input-w', default=0, type=int, help='override specified w')
    parser.add_argument('--override-input-h', default=0, type=int, help='override specified h')
    parser.add_argument('--num-workers', default=32, type=int, help='Number of workers used in dataloading')
    parser.add_argument('--no-create-version', action="store_true", help='run, but do not create a new version')
    parser.add_argument('--upload-destination', default='s3://allegro-examples', type=str,
                        help='Destination to upload debug images and models')

    # new additions, pending refactoring
    # source id
    parser.add_argument('--source-id', type=str, default=None,
                        help='In case of several sources per frame, only choose this source id to annotate on')

    # ========= (1) Define task ==============================
    task = Task.current_task(default_project_name=PROJECT_NAME, default_task_name=TASK_NAME,
                             default_task_type=tasks.TaskTypeEnum.testing)
    seed = task.get_random_seed()
    make_deterministic(seed)  # setup random seed from task for reproducibility

    #################################
    # Connect arguments to the task #
    #################################
    task.connect(parser)
    args = parser.parse_args()
    if args.source_id == '':
        args.source_id = None

    logger = task.get_logger()
    logger.set_default_upload_destination(uri=args.upload_destination)
    logger.console('Running arguments: %s' % str(args))

    # ========= (2) Define input model, its labels and config  ==============================
    INPUT_MODEL_URL = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_Person_Detector.pth'
    INPUT_MODEL_NAME = 'Pytorch SSD person detector'
    input_model = InputModel.import_model(weights_url=INPUT_MODEL_URL, name=INPUT_MODEL_NAME,
                                          design=None, label_enumeration=None)
    task.connect(input_model)
    # either input model or task itself must have a design
    net_design = task.get_model_design() or input_model.design
    if not net_design or len(net_design) == 0:
        raise IOError('A network design must be present in order to load model. got %s' % net_design)
    config_params = yaml.load(net_design)

    # either input model or task itself must have labels
    model_labels = task.get_labels_enumeration() or input_model.labels
    if not model_labels or all([value <= 0 for value in model_labels.values()]):
        default_labels = {'background': 0,  'person': 1}
        logger.console('No input model labels, or no positive ids : {} ,'
                       'using default labels for this script'.format(model_labels), level=ERROR)
        model_labels = default_labels

    if args.create_at_conf_thresh > 0:
        previous_conf = config_params.pop('test_detection_conf_thresh', 'Not Specified')
        config_params['test_detection_conf_thresh'] = float(args.create_at_conf_thresh)
        logger.console('Override: confidence thresh for inference is now %.2f instead of %s' %
                       (config_params['test_detection_conf_thresh'], str(previous_conf)))
    if args.override_input_w > 0:
        previous_w = config_params['min_dim_w']
        config_params['min_dim_w'] = args.override_input_w
        logger.console('Override: input width is now %d instead of %d' %
                       (config_params['min_dim_w'], previous_w))
    if args.override_input_h > 0:
        previous_h = config_params['min_dim_h']
        config_params['min_dim_h'] = args.override_input_h
        logger.console('Override: input width is now %d instead of %d' %
                       (config_params['min_dim_h'], previous_h))

    # ========= (3) Define dataview  ==============================
    # Note: you cannot annotate a public dataset, so this task requires that you add your own dataset and version
    dataview = DataView(iteration_order=IterationOrder.sequential, iteration_infinite=False)
    DATASET_NAME = 'Tutorial'  # Please replace with a non public dataset
    VERSION_NAME = 'Data registration'  # Please replace with a non public version

    try:
        dataview.add_query(dataset_name=DATASET_NAME, version_name=VERSION_NAME,
                           filter_by_roi=FilterByRoi.disabled)
    except Exception as ex:
        logger.console(str(ex), ERROR)
    task.connect(dataview)

    # update labels here if needed, these are taken from the input model labels:
    id_to_model_label = {v: k for k, v in model_labels.items()}

    # ========= (4) Validate input ==============================
    # Check that dataset and version details are valid...
    all_versions = dataview._dataview.data.versions
    if len(all_versions) != 1:
        num_queries = len(all_versions)
        raise ValueError('This task is designed for a single dataset query only in order to create a version'
                         ', received {} different versions/datasets'.format(num_queries))

    orig_version = DatasetVersion.get_version(
        dataset_id=all_versions[0].dataset,
        version_id=all_versions[0].version
    )
    logger.console('Found version: %s' % orig_version.version_name)

    # ========= (5) Create new target version ==============================
    if args.no_create_version:
        raise NotImplemented('Inference on a single version without creation of a new one is still not available')

    if not args.new_version_name:
        args.new_version_name = ' | '.join([orig_version.version_name, task.name + ' - ' +
                                            str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))])
    logger.console('Creating new dataset version: %s' % args.new_version_name)
    try:
        target_version = DatasetVersion.create_version(
            name=args.new_version_name,
            dataset_id=orig_version.dataset_id,
            parent_version_ids=[orig_version.version_id]
        )
    except Exception as ex:
        from logging import ERROR
        msg = 'Dataset version creation failed. Are you sure you are able to edit this dataset?'
        logger.console(str(ex), level=ERROR, exc_info=True)
        logger.console(msg, level=ERROR)
        logger.flush()
        raise PermissionError(msg)
    # ========= (6) Build the network ==============================
    #
    target_image_size = ImageSizeTuple(w=config_params['min_dim_w'], h=config_params['min_dim_h'])
    cuda_is_on = setup_pytorch03(args=None, logger=logger)
    net = build_net(cfg=config_params, training=False,
                    args=args, input_image_size=target_image_size, use_cudnn=cuda_is_on)
    weights_file = input_model.get_weights()
    logger.console('Loading net weights from {}...'.format(weights_file))
    net.ssd.forgiving_load(weights_file, logger=logger)

    orig_iterator = dataview.get_iterator()

    if args.source_id:
        source_id = [args.source_id]
    else:
        source_id = None
    pipe = ScanningDataPipe(iterator=orig_iterator, num_workers=args.num_workers,
                            frame_cls_kwargs=dict(crop_width=target_image_size.w,
                                                  crop_height=target_image_size.h,
                                                  overlap_percent=DEFAULT_OVERLAP_RATIO,
                                                  default_source_ids_to_load=source_id))

    # ========= (7) Fill each frame with prediction results ==============================
    #
    iterator = pipe.get_iterator()

    with target_version.get_bulk_context() as edit:
        last_meta = None
        scanned_img = None
        prev_rois = report_prev_rois = report_boxes = report_image = report_scores = report_meta = None
        all_batch_boxes = all_batch_scores = all_ground_truth = all_hard_boxes = None

        pre_batch_collect = []
        report_iter = 0
        for i, image_frame in enumerate(iterator):
            frame_process_timer.tic()
            frame_get_image_timer.tic()
            meta_frame = image_frame.get_meta_data()
            all_sources = image_frame.get_sources()
            if args.source_id:
                sources = [args.source_id]
            else:
                sources = [all_sources[0]]
            for source in sources:
                upper_left_xy = image_frame._upper_left_xy
                meta_frame = image_frame.get_meta_data()
                width = meta_frame.width
                height = meta_frame.height
                if upper_left_xy == [0, 0]:
                    if i > 0:
                        report_prev_rois = copy(prev_rois)
                        report_meta = copy(last_meta)
                        report_image = scanned_img.copy() if scanned_img is not None else None
                        report_boxes = all_batch_boxes.copy() if all_batch_boxes is not None else None
                        report_scores = all_batch_scores.copy() if all_batch_scores is not None else None
                    scanned_img = np.zeros(shape=(height, width, 3)).astype('uint8')
                    all_batch_boxes = None
                    all_batch_scores = None
                    all_ground_truth = None
                    all_hard_boxes = None

                prev_rois = []
                if len(all_sources) > 1:
                    for src in all_sources:
                        if src not in sources:
                            curr_rois = [roi for roi in meta_frame.rois if src in roi.sources]
                            prev_rois = prev_rois + curr_rois
                last_meta = meta_frame
                scanned_img[
                    upper_left_xy[1]:upper_left_xy[1] + pipe._frame_cls_kwargs['crop_height'],
                    upper_left_xy[0]:upper_left_xy[0] + pipe._frame_cls_kwargs['crop_width'], :] = \
                    image_frame.get_data()

                im = image_frame.get_data(source_id=source).astype(np.float32)
                image = im.transpose(2, 0, 1)
                img_tensor = np.zeros((1, 3) + tuple(target_image_size)[::-1], dtype=np.float32)
                d1, d2, d3 = image.shape
                img_tensor[0, :d1, :d2, :d3] = image
                frame_get_image_timer.toc()

                frame_inference_timer.tic()
                curr_batch_boxes, curr_batch_scores = ssd_inference_on_image(img_tensor)
                frame_inference_timer.toc()

                if curr_batch_boxes is not None:
                    shifted_boxes = curr_batch_boxes.copy()
                    shifted_boxes[:, 1:5] += upper_left_xy * 2
                    if all_batch_boxes is None:
                        all_batch_boxes = shifted_boxes
                        all_batch_scores = curr_batch_scores
                    else:
                        all_batch_boxes = np.vstack((all_batch_boxes, shifted_boxes))
                        all_batch_scores = np.hstack((all_batch_scores, curr_batch_scores))

                if upper_left_xy == [0, 0] and i > 0:
                    report_iter += 1
                    img_rois, report_boxes_after_nms = prepare_report_rois(report_boxes, report_scores)

                    if report_iter > 0 and report_iter % args.debug_image_freq == 0:
                        img = report_image.copy()
                        if report_boxes_after_nms is not None:
                            boxes = report_boxes_after_nms
                            for n, b in enumerate(boxes):
                                ann = [id_to_model_label.get(b[5])]
                                C = (0, 0, 0)
                                b = np.array(b).astype('int')
                                cv2.rectangle(img, pt1=tuple(b[1:3]), pt2=tuple(b[3:5]), color=C, thickness=2)
                        logger.report_image_and_upload(title='Inference result', series='img_%d' % report_iter,
                                                       iteration=report_iter, matrix=img.astype(np.uint8))
                        logger.flush()

                    report_meta.rois = report_prev_rois + img_rois
                    if not report_meta.meta:
                        report_meta.meta = {}
                    report_meta.meta['auto_annotate'] = '1'
                    edit.update_frame(report_meta)
                    frame_process_timer.toc()
                    if report_iter > 0 and report_iter % 10 == 0:
                        t_process = frame_process_timer.average_time
                        t_get_image = frame_get_image_timer.average_time
                        t_inference = frame_inference_timer.average_time
                        logger.console(
                            'Currently at frame %d, t_frame: %.3f (sec) t_inference: %.3f (sec) t_overhead: %.3f (sec)' %
                            (report_iter, t_process, t_inference, t_process - t_get_image - t_inference))

        # Report last frame
        report_boxes = all_batch_boxes.copy() if all_batch_boxes is not None else None
        report_scores = all_batch_scores.copy() if all_batch_scores is not None else None
        img_rois, __ = prepare_report_rois(report_boxes, report_scores)
        report_prev_rois = copy(prev_rois)
        report_meta = copy(last_meta)
        report_meta.rois = report_prev_rois + img_rois
        if not report_meta.meta:
            report_meta.meta = {}
        report_meta.meta['auto_annotate'] = '1'
        edit.update_frame(report_meta)

        logger.console('Sending frames')

    # (7.1) Commit
    logger.console('Committing new dataset version: %s' % args.new_version_name)
    commit_result = target_version.commit_version()
    logger.console('Commit result %s ' % commit_result.response)

    # (7.2) Publish
    target_version.publish_version()
    logger.console('New dataset version published')
    logger.console('Done')
