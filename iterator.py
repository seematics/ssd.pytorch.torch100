from allegroai import Task, DataView

if __name__ == '__main__':
    task = Task.current_task()
    log = task.get_logger()
    dv = DataView()
    dv.add_query(dataset_id="0fb4381c26264ecca8a822fd845ef948", version_id="14c8dffbcde042a4a26f725631a2e676")
    task.connect(dv)
    it = dv.get_iterator()
    try:
        next(it)
        log.console("Got thas frame")
    except StopIteration:
        log.console("Got stop iteration")
    finally:
        it.close()
