import copy
import logging
import os
from collections import namedtuple
from tempfile import mkstemp
import argparse
import random

import numpy as np
import torch
from allegroai.utilities.images import ResizeStrategy
from pathlib2 import Path
from torch.optim.lr_scheduler import _LRScheduler

from allegroai import ImageFrame
from allegroai.config import running_remotely

from models.ssd_vgg import build_ssd_with_vgg
from models.ssd_squeezenet import build_ssd_with_squeezenet

from layers import Detect, DragonLoss

ImageSizeTuple = namedtuple('ImageSizeTuple', 'w h')


def make_deterministic(seed):
    # setup radnom seeds
    # cudnn.deterministic = True
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)


def build_net(cfg, args, input_image_size, use_cudnn=True, training=True):
    variant = 512 if max(input_image_size) >= 400 else 300
    ssd_net, freeze_n = build_ssd(args, cfg, variant, input_image_size)

    # wrap to enable training on multiple GPUs
    net = torch.nn.DataParallel(ssd_net) if use_cudnn else ssd_net

    decode_boxes = Detect(num_classes=cfg['num_classes'], bkg_label=0,
                          top_k=cfg.get('top_k', 200),
                          conf_thresh=cfg.get('conf_thresh', 0.01),
                          nms_thresh=cfg['nms_thresh'], cfg=cfg)

    softmax = torch.nn.Softmax(dim=-1)

    if training:
        end_to_end = cfg.get('end_to_end', False)
        # we spcifically state require_grad = False for bottom vgg layers
        if not end_to_end:
            freeze_first_n_layers(ssd_net.features, freeze_n)

        criterion = DragonLoss(num_classes=cfg['num_classes'],
                               overlap_thresh=cfg['multi_box_loss_overlap_thresh'],
                               prior_for_matching=True,
                               bkg_label=cfg['background_label'],
                               neg_mining=True,
                               neg_pos=cfg['negative_positive_ratio'],
                               neg_overlap=0.5,
                               encode_target=False,
                               variance=cfg['variance'],
                               min_bkg=cfg['min_number_of_background_priors'],
                               hard_neg_mix=cfg['hard_negative_mix_in_background'],
                               use_gpu=use_cudnn)

        params = filter(lambda p: p.requires_grad, net.parameters())

        optimizer = torch.optim.SGD(params, lr=cfg['lr_list'][0],
                                    momentum=args.momentum, weight_decay=args.weight_decay)

        lr_scheduler = CyclicLRScheduler(optimizer=optimizer,
                                         upper_bound_steps=cfg['lr_steps'],
                                         lr_list=cfg['lr_list'])
    else:
        criterion = None
        optimizer = None
        lr_scheduler = None

    if not training:
        ssd_net.eval()

    net_components = namedtuple('SSDComponents', ['ssd', 'data_parallel', 'decode_boxes', 'softmax', 'optimizer', 'lr_scheduler', 'loss'])

    return net_components(ssd_net, net, decode_boxes, softmax, optimizer, lr_scheduler, criterion)


def build_ssd(args, config_params, variant, target_image_size):
    if 'dropout' not in vars(args):
        args.dropout = 0.1
    if args.feature_extraction_type == 'vgg16':
        freeze_n = 21
        ssd_net = build_ssd_with_vgg(cfg=config_params, phase='train',
                                     variant=variant, size=target_image_size,
                                     num_classes=config_params['num_classes'], vgg_type='vgg16', dropout=args.dropout)
    elif args.feature_extraction_type == 'vgg19':
        freeze_n = 25
        ssd_net = build_ssd_with_vgg(cfg=config_params, phase='train',
                                     variant=variant, size=target_image_size,
                                     num_classes=config_params['num_classes'], vgg_type='vgg19', dropout=args.dropout)
    elif args.feature_extraction_type == 'squeezenet1_0':
        freeze_n = 10
        ssd_net = build_ssd_with_squeezenet(cfg=config_params, phase='train',
                                            variant=variant, size=target_image_size,
                                            num_classes=config_params['num_classes'], version=1.0, dropout=args.dropout)
    elif args.feature_extraction_type == 'squeezenet1_1':
        freeze_n = 7
        ssd_net = build_ssd_with_squeezenet(cfg=config_params, phase='train',
                                            variant=variant, size=target_image_size,
                                            num_classes=config_params['num_classes'], version=1.1, dropout=args.dropout)
    elif args.feature_extraction_type == 'resnet50':
        raise ValueError('"resnet50" is not supported at the moment')
        # freeze_n = 16
        # ssd_net = build_ssd_with_resnet(cfg=config_params, phase='train',
        #                                 variant=variant, size=target_image_size,
        #                                 num_classes=config_params['num_classes'], resnet_type='resnet50')
    else:
        freeze_n = 0
        raise ValueError('Unsupported feature extraction type {}'.format(args.feature_extraction_type))

    return ssd_net, freeze_n


def bound_number_type(minimum=None, maximum=None, dtype=float):
    """
    bound_number_type

    Creates a bounded integer "type" (validator function)
    for use with argparse.ArgumentParser.add_argument.
    At least one of ``minimum`` and ``maximum`` must be passed.

    :param minimum: maximum allowed value
    :param maximum: minimum allowed value
    :param dtype: argument type (either float or int)
    """

    if minimum is maximum is None:
        raise ValueError('either "minimum" or "maximum" must be provided')

    def bound_value(arg):
        num = dtype(arg)

        if minimum is not None and num < minimum:
            raise argparse.ArgumentTypeError('minimum value is {}'.format(minimum))
        if maximum is not None and num > maximum:
            raise argparse.ArgumentTypeError('maximum value is {}'.format(minimum))
        return num

    return bound_value


def get_model_url(feature_extraction_type, only_feature_extraction=False):
    if only_feature_extraction:
        # using pure model zoo:
        if feature_extraction_type == 'vgg16':
            url = 'https://download.pytorch.org/models/vgg16-397923af.pth'
            name = 'torchvision model zoo VGG16'
        elif feature_extraction_type == 'vgg19':
            url = 'https://download.pytorch.org/models/vgg19-dcbb9e9d.pth'
            name = 'torchvision model zoo VGG19'
        elif feature_extraction_type == 'squeezenet1_0':
            url = 'https://download.pytorch.org/models/squeezenet1_0-a815701f.pth'
            name = 'torchvision model zoo SqueezeNet version 1.0'
        elif feature_extraction_type == 'squeezenet1_1':
            url = 'https://download.pytorch.org/models/squeezenet1_1-f364aa15.pth'
            name = 'torchvision model zoo SqueezeNet version 1.1'
        elif feature_extraction_type == 'resnet50':
            url = 'https://download.pytorch.org/models/resnet50-19c8e357.pth'
            name = 'torchvision model zoo ResNet50'
        else:
            raise ValueError('Feature extraction type can be only one of the following: vgg16, vgg19, squeezenet1_0,'
                             'squeezenet1_1, resnet50')
    else:
        if feature_extraction_type == 'vgg16':
            # 'https://download.pytorch.org/models/vgg16-397923af.pth'
            url = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_VGG16_trained_on_VOC2012.pth'
            name = 'Pytorch SSD | feature extraction - torchvision model zoo VGG16 | classifier - trained on VOC'
        elif feature_extraction_type == 'vgg19':
            # 'https://download.pytorch.org/models/vgg19-dcbb9e9d.pth'
            url = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_VGG19_trained_on_VOC2012.pth'
            name = 'Pytorch SSD | feature extraction - torchvision model zoo VGG19 | classifier - trained on VOC'
        elif feature_extraction_type == 'squeezenet1_0':
            # 'https://download.pytorch.org/models/squeezenet1_0-a815701f.pth'
            url = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_SqueezeNet1_0_trained_on_VOC2012.pth'
            name = 'Pytorch SSD | feature extraction - torchvision model zoo SqueezeNet 1.0 |' \
                   ' classifier - trained on VOC'
        elif feature_extraction_type == 'squeezenet1_1':
            url = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_SqueezeNet1_1_trained_on_KITTI.pth'
            name = 'torchvision model zoo SqueezeNet version 1.1'
        elif feature_extraction_type == 'resnet50':
            url = 'https://download.pytorch.org/models/resnet50-19c8e357.pth'
            name = 'torchvision model zoo ResNet50'
        else:
            raise ValueError('Feature extraction type can be only one of the following: vgg16, vgg19, squeezenet1_0,'
                             'squeezenet1_1, resnet50')

    return url, name


def resolve_output_labels(task_labels, script_labels):
    if running_remotely():
        output_model_labels = task_labels
    else:
        output_model_labels = script_labels

    # Sanity check for the final labels:
    positive_model_labels = [v for _, v in output_model_labels.items() if v > 0]
    model_num_positive_classes = len(positive_model_labels)
    if not model_num_positive_classes:
        raise ValueError('No positive label id defined for the model? currently : %s' % str(output_model_labels))
    elif model_num_positive_classes != max(positive_model_labels):
        raise ValueError('Not all model labels have a defined id, cannot run task. num id >0 : '
                         '%d while largest id is %d' % (model_num_positive_classes, max(positive_model_labels)))

    return output_model_labels


def adj_mean_of_image_batch(images, bgr_means):
    bgr_means = np.array(bgr_means)
    images += bgr_means
    return images


def freeze_first_n_layers(nn_module, n=21):
    for i, param in enumerate(nn_module.parameters()):
        if i < n:
            param.requires_grad = False
        if i > n:
            break


def ssd_output_to_allegro_format(numpy_ssd_output, conf_thresh, img_width, img_height):
    """
    Transform ssd output to Seematics format of bounding boxes
    Providing None as img width and height will return relative coordinates for the boxes
    :param numpy_ssd_output: (batch_size, num_classes, num_boxes, 5) - each v \in R^5 is (cls_score, x1, y1, x2, y2)
                             we have a v for every box class and batch imagez
    :param conf_thresh: minimum confidence for transformation
    :param img_width: original image width (x1y1x2y2 boxes are in relatinve coords)
    :param img_height: original image height (x1y1x2y2 boxes are in relatinve coords)

    :return:
        all_batch_boxes - (num_boxes, 5) wehere each v \in R^5 is (batch_id, x1, y1, x2, y2, label)
        all_batch_scores - (num_boxes, ) of scores
    """

    all_batch_boxes = []
    all_batch_scores = []

    for bidx in range(numpy_ssd_output.shape[0]):
        cls_id, box_id = np.where(numpy_ssd_output[bidx, :, :, 0] > conf_thresh)
        b_dets = numpy_ssd_output[bidx, cls_id, box_id, :]
        b_dets = np.minimum(1., np.maximum(0., b_dets))
        if not b_dets.size:
            continue
        b_dets = np.hstack([np.ones((b_dets.shape[0], 1)) * bidx, b_dets])
        b_dets = np.atleast_2d(b_dets)[:, [0, 2, 3, 4, 5, 1]]

        if img_width is not None and img_height is not None:
            b_dets[:, 1:5:2] *= img_width
            b_dets[:, 2:5:2] *= img_height

        scores = b_dets[:, 5]
        pred_boxes = np.hstack([b_dets[:, :5], cls_id[:, np.newaxis]])
        all_batch_boxes.append(pred_boxes)
        all_batch_scores.append(scores)

    all_batch_boxes = np.vstack(all_batch_boxes) if len(all_batch_boxes) else None
    all_batch_scores = np.concatenate(all_batch_scores) if len(all_batch_scores) else None

    return all_batch_boxes, all_batch_scores


class CyclicLRScheduler(_LRScheduler):

    def __init__(self, optimizer, upper_bound_steps, lr_list, last_epoch=-1):
        self._total_cycle_len = sum(upper_bound_steps)
        self._upper_bounds = upper_bound_steps
        self._lrs = lr_list
        self._lr_length = len(optimizer.param_groups)
        self._buckets = copy.copy(upper_bound_steps)
        self._bucket_index = 0
        super(CyclicLRScheduler, self).__init__(optimizer=optimizer, last_epoch=last_epoch)

    def get_lr(self):
        cur_bucket_size = self._buckets[self._bucket_index]
        if cur_bucket_size < 1:
            self._bucket_index += 1
            if self._bucket_index == len(self._upper_bounds):
                self._bucket_index = 0
                self._buckets = copy.copy(self._upper_bounds)

        self._buckets[self._bucket_index] -= 1

        return [self._lrs[self._bucket_index]] * self._lr_length


def setup_pytorch03(args, logger=None, use_cudnn=True, suppress_warnings=True):
    """
    Pytorch 0.3 boilerplate
    :param suppress_warnings: if running on pytorch 0.4 you probably would want to suppress warnings
    :param args: ArgParser object
    :param logger: logger object (for console output)
    :param use_cudnn: True to use cuda, False for cpu mode
    :return: cuda state (True if on)
    """
    cuda_state_from_args = args.cuda if hasattr(args, 'cuda') else use_cudnn
    if cuda_state_from_args and torch.cuda.is_available():
        if use_cudnn:
            torch.set_default_tensor_type('torch.cuda.FloatTensor')
        else:
            logger.console("It looks like you have a CUDA device, but aren't " +
                           "using CUDA.\nRun with --cuda for optimal training speed.", level=logging.WARNING)
            torch.set_default_tensor_type('torch.FloatTensor')
            use_cudnn = False
    else:
        torch.set_default_tensor_type('torch.FloatTensor')
        if torch.cuda.is_available():
            logger.console("Not using GPU for running pytorch", level=logging.WARNING)
        use_cudnn = False

    if use_cudnn:
        cudnn_version = torch.backends.cudnn.version()
        if cudnn_version:
            logger.console('Using CUDNN version %s' % cudnn_version)
            torch.backends.cudnn.benchmark = True
            logger.console('Enabled optimization for fixed input size')
        else:
            logger.console('CUDNN backend not available!', level=logging.WARNING)

    if suppress_warnings:
        # TODO hack until we fully merge to pytorch 0.4
        import warnings
        warnings.filterwarnings(action="ignore", message="volatile.*")

    return use_cudnn


class CompatibilityWrapper(object):
    def __init__(self, im, image_frame):
        self.im = im
        self.image_frame = image_frame

    def get_boxes(self):
        return np.array([ImageFrame.get_polygon_bounding_box(f['points'])
                         for f in self.image_frame.get_polygons()])

    def get_polygons(self):
        return [f['points'] for f in self.image_frame.get_polygons()]

    def get_annotations(self):
        return [f['labels'] for f in self.image_frame.get_polygons()]

    def get_image(self):
        return self.im


def yaml_file_to_text(path_to_yaml):
    p = Path(path_to_yaml)
    return p.open('r').read()


def upload_model_snapshot(net, model, iteration):
    fd, filename = mkstemp(suffix=('.%d.pth' % iteration))
    torch.save(net.state_dict(), open(filename, 'wb'))
    os.close(fd)
    # assert model.is_output_model, "only output models can be updated"
    model.update_weights(filename)


def normalize_boxes(boxes, img_w, img_h):
    # Assumes batch id is the first element of roi
    boxes = np.array(boxes)
    assert boxes.shape[1] == 4, 'Expect boxes of shape (N, 4)'
    boxes[:, ::2] /= float(img_w)
    boxes[:, 1::2] /= float(img_h)
    return boxes.astype(np.float32)


def jitter_box_and_score(box, score, crop=0.6, jitter_span=0.):
    crop /= 2.
    dx, dy = ((box[2:] - box[:2]) * crop).astype(np.int)
    dd = int((2 * np.random.rand() - 1) * min(dx, dy) * jitter_span)
    box += [dx, dy, -dx, -dy]
    box += dd
    return box, score


def resolve_resize_strategy(parsed_args, logger):
    parsed_dict = vars(parsed_args)
    strategies = {
        'always': ResizeStrategy.RESIZE_ALWAYS,
        'keep_ar': ResizeStrategy.ALWAYS_KEEP_ASPECT_RATIO,
        'bigger_keep_ar': ResizeStrategy.BIGGER_KEEP_ASPECT_RATIO,
        'default_for_ssd': ResizeStrategy.BIGGER_KEEP_ASPECT_RATIO
    }
    if 'resize_strategy' in parsed_dict:
        my_strat = parsed_dict['resize_strategy']
        if my_strat not in strategies:
            logger.warn('"{}" not in supported strategies: {}, using DEFAULT.'.
                        format(my_strat, [str(k) for k in strategies.keys()]))
        return strategies.get(my_strat, strategies['default_for_ssd'])
    else:
        # backwards compatibility
        return ResizeStrategy.BIGGER_KEEP_ASPECT_RATIO
