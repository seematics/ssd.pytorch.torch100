import cv2
import datetime
from argparse import ArgumentParser
from collections import namedtuple

import yaml
import numpy as np

from allegroai import DatasetVersion, DataView, Task, Model
from allegroai.dataview import FilterByRoi
from allegroai.imageframe import ImageFrame
from allegroai.dataview import IterationOrder
from allegroai_api.services import frames

from setup_ssd import build_net
from common.utils import setup_pytorch03

ImageSizeTuple = namedtuple('ImageSizeTuple', 'w h')
INPUT_MODEL_ID = 00000
def yssd_inference_on_image(image, config_params):
    return all_batch_boxes, all_batch_scores


def box_to_poly(box):
    top_left = [box[0], box[1]]
    top_right = [box[2], box[1]]
    bottom_right = [box[2], box[3]]
    bottom_left = [box[0], box[3]]
    poly = np.hstack((top_left, top_right, bottom_right, bottom_left)).tolist()
    return poly


if __name__ == '__main__':
    parser = ArgumentParser(description='Create dataset version')
    parser.add_argument('--new-version-name', help='Name for the generated version')

    # ========= (1) Define task ==============================
    task = Task.current_task(default_project_name='YSSD version maker test',
                             default_task_name='Try suggested annotation')
    task.connect(parser)
    args = parser.parse_args()

    logger = task.get_logger()
    logger.set_default_upload_destination(uri='s3://seematics-examples/bosch')

    # ========= (2) Define input model  ==============================
    input_model = Model.load_model(model_id=INPUT_MODEL_ID)
    task.connect(input_model)

    # ========= (3) Define dataview  ==============================
    dataview = DataView()
    dataview.add_query(dataset_name='^SAIVT',
                       version_name='movie from 15:00 to 16:00',
                       filter_by_roi=FilterByRoi.disabled)
    dataview.set_iteration_parameters(order=IterationOrder.sequential, infinite=False)
    task.connect(dataview)

    # ========= (4) Main  ==============================
    # build the network
    cuda_is_on = setup_pytorch03(args=None, logger=logger)
    model = Model(task=task)
    model.set_upload_destination(uri='s3://seematics-examples/bosch')
    model.update_design(ssd_create_cfg())
    config_params = yaml.load(model.get_design())
    target_image_size = ImageSizeTuple(w=config_params['min_dim_w'], h=config_params['min_dim_h'])
    net = build_net(cfg=config_params, args=None, input_image_size=target_image_size, training=False)
    weights_file = input_model.get_weights()
    logger.console('Resuming training, loading {}...'.format(weights_file))
    net.ssd.forgiving_load(weights_file, logger=logger)
    net.data_parallel.eval()

    # Check that dataset and version details are valid
    input_datasets = dataview.get_queries()
    if len(input_datasets) != 1:
        raise ValueError('Too many datasets input versions we support single version only')

    orig_version = DatasetVersion.get_version(
        dataset_id=input_datasets[0].dataset,
        version_id=input_datasets[0].version
    )
    logger.console('Found version: %s' % orig_version.version_name)

    # create new target version
    if not args.new_version_name:
        args.new_version_name = ' | '.join([orig_version.version_name, task.name, 'suggested annotation - ' +
                                            str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))])

    logger.console('Creating new dataset version: %s' % args.new_version_name)
    target_version = DatasetVersion.create_version(
        name=args.new_version_name,
        dataset_id=orig_version.dataset_id,
        parent_version_ids=[orig_version.version_id]
    )

    orig_iterator = dataview.get_iterator()

    with target_version.get_update_context() as edit:
        for i, frame in enumerate(orig_iterator):
            meta_image = ImageFrame(frame, target_width_height=(target_image_size.w, target_image_size.h))
            im = meta_image.get_data().astype(np.float32)
            image = im.transpose(2, 0, 1)
            img_tensor = np.zeros((1, 3) + tuple(target_image_size)[::-1], dtype=np.float32)
            d1, d2, d3 = image.shape
            img_tensor[0, :d1, :d2, :d3] = image
            all_batch_boxes, all_batch_scores = ssd_inference_on_image(img_tensor, config_params)

            resize_box_factors = [frame.width/d3, frame.height/d2]*2
            img_rois = []
            if all_batch_boxes is not None:
                for n, bbox in enumerate(all_batch_boxes):
                    box = bbox[1:5]
                    label = bbox[5]
                    score = all_batch_scores[n]
                    img_rois.append(frames.Roi(
                        # Todo implement mapping instead hard coded label
                        label=['person'],
                        poly=box_to_poly(list(box*resize_box_factors)),
                        confidence=score))

            frame.rois = img_rois
            if not frame.meta:
                frame.meta = {}
            frame.meta['auto_annotate'] = '1'
            # send the frame
            edit.update_meta_frame(frame)
            if i > 0 and i % 500 == 0:
                logger.console('%d' % i)
                if all_batch_boxes is not None:
                    img = im.copy()
                    ann = ['person']*len(all_batch_boxes)
                    boxes = all_batch_boxes
                    for n, b in enumerate(boxes):
                        C = (0, 0, 0)
                        b = np.array(b).astype('int')
                        cv2.rectangle(img, pt1=tuple(b[1:3]), pt2=tuple(b[3:5]), color=C, thickness=2)
                    logger.report_image_and_upload(title='Predictions', series='img_%d' % i, iteration=i,
                                                   matrix=img.astype(np.uint8))
                    logger.flush()
        logger.console('Sending frames')

    # Commit
    logger.console('Committing new dataset version: %s' % args.new_version_name)
    commit_result = target_version.commit_version()
    logger.console('Commit result', commit_result)

    # Publish
    target_version.publish_version()
    logger.console('New dataset version published')
    logger.console('Done')
