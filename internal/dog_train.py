import torch
import numpy as np
import cv2
import time
import yaml

from argparse import ArgumentParser
from functools import partial
from logging import ERROR, WARNING
from pathlib2 import Path
from torch.autograd import Variable

from common.metrics import BoxesHistory, average_precision_from_curve
from common.utils import ssd_output_to_allegro_format, setup_pytorch03, ImageSizeTuple, upload_model_snapshot
from common.utils import CyclicLRScheduler, freeze_first_n_layers
from common.loading import default_weight_init_for_module
from common.utils import get_model_url, resolve_output_labels, update_dataview_labels, dataview_exists_in_project
from common.utils import bound_number_type
from common.visualization import DrawBoxesAndLabels

from allegroai.debugging.timer import Timer
from allegroai.imageframe import ResizeStrategy
from allegroai import DataView, Augmentation, DataPipe, Task, InputModel, OutputModel
# from allegroai.utilities.augmentations import CustomAugmentationZoom

from common.batchers import make_ssd_batch

from layers import Detect, DragonLoss
from models.ssd_vgg import build_ssd_with_vgg
from models.ssd_squeezenet import build_ssd_with_squeezenet

utils = namedtuple('Utils','softmax decode_boxes')


class SSDExperiment:

    def __init__(self, project_name, task_name, train_dataview, label_mapping=None, args_override=None, net_design_override=None, input_model=None):
        self.project_name = project_name
        self.task_name = task_name
        self.train_dataview = train_dataview
        self.label_mapping = label_mapping or {}
        self.input_model = input_model

        self.utils = None
        # Debug printing - set to true to report how many positive/negative examples were seen for that specific batch
        self.debug_report_loss_internals = True
        # for accurate timing, we reset the timers every 10 reports. (set to, e.g., -1 to disable this feature)
        self.timers_benchmark_windows = 10

        self.task = Task.current_task(default_project_name=self.project_name, default_task_name=self.task_name)

        connect_to_task_parser = self._get_parser()
        if args_override:
            connect_to_task_parser.set_defaults(**args_override)

        self.task.connect(connect_to_task_parser)
        self.args = connect_to_task_parser.parse_args()

        self.logger = self.task.get_logger()
        self.logger.set_default_upload_destination(uri=self.args.upload_destination)
        self.logger.console('Running arguments: %s' % str(self.args))
        self.logger.flush()

        self._configure_experiment()
        self._setup_network()

    def run(self):
        self.logger.console(">>> Running the experiment")
        if self.test_while_train and (self.args.test_before_train or self.args.only_test):
            self.task.mark_started()
            self._test()

        if not self.args.only_test:
            start_at = 0
            stop_at = self.args.max_iterations
            self._train(start_at, stop_at)

    def _configure_experiment(self):
        logger = self.logger

        logger.console(">>> Configuring the experiment")

        ##################################################
        # Defining input model connecting it to the task #
        ##################################################
        # Here we import a model from a url using import_model, since the user might not have a registered model yet
        if self.input_model is None:
            input_model_url, input_model_name = get_model_url(self.args.feature_extraction_type)
            self.input_model = InputModel.import_model(
                weights_url=input_model_url,
                name=input_model_name,
                design=None,
                label_enumeration=None
            )
            self.input_model.publish()
        self.task.connect(self.input_model)

        #########################
        # Defining output model #
        #########################
        self.output_model = OutputModel(task=self.task)
        self.output_model.set_upload_destination(uri=self.args.upload_destination)

        if not self.output_model.design:
            self.logger.console('Loading default design into output model')
            default_design_file = Path('ssd_design.yaml')
            if not default_design_file.exists():
                raise IOError('Missing model design file in repo {}'.format(default_design_file))
            self.output_model.update_design(default_design_file.read_text())

        # Training dataview
        if self.train_dataview:
            self.task.connect(self.train_dataview)
        elif not self.args.only_test:
            raise ValueError('Missing training dataview')

        ##################################################
        # Defining task's labels (class name to integer) #
        ##################################################
        # Map objects to hard in test dataview, so the model do not get penalty for not detecting these objects
        task_labels = self.task.get_labels_enumeration()
        self.output_model_labels = resolve_output_labels(task_labels, self.label_mapping)
        self.output_model.update_labels(self.output_model_labels)
        if self.train_dataview:
            self.train_dataview.set_labels(self.output_model_labels)  # Has no effect in remote mode

        #########################################
        # Adding auxiliary dataview as test set #
        #########################################
        if dataview_exists_in_project(self.args.test_dataview_id, self.task.project):
            update_dataview_labels(dataview_id=self.args.test_dataview_id, new_labels=self.output_model_labels)
            self.test_dataview = DataView(load_dataview_id=self.args.test_dataview_id)
            logger.console('Using test dataview id "%s"' % self.args.test_dataview_id)
            self.test_while_train = True
        else:
            logger.console('Test dataview id "%s" was not found - deactivating test while train'
                           % self.args.test_dataview_id, level=ERROR)
            self.test_while_train = False

    def _setup_network(self):
        logger = self.logger
        args = self.args
        logger.console(">>> Setting up the network")

        ######################################
        # Reading model design configuration #
        ######################################
        config_params = yaml.load(self.output_model.design)

        self.target_image_size = ImageSizeTuple(w=config_params['min_dim_w'], h=config_params['min_dim_h'])

        #############################################################
        # Build the SSD net, adjusting for number of labels/classes #
        #############################################################
        # Adjust num-classes if there are more model labels than classes
        model_num_positive_classes = len([v for _, v in self.output_model_labels.items() if v > 0])
        design_num_classes = config_params.setdefault('num_classes', 1 + model_num_positive_classes)
        if design_num_classes != 1 + model_num_positive_classes:
            logger.console('Model design contained different number of classes than expected. Fixing to %d classes' % (
                    1 + model_num_positive_classes), WARNING)
            config_params['num_classes'] = 1 + model_num_positive_classes
            self.output_model.update_design(yaml.dump(config_params))
            logger.console('If you are purposely changing the number of classes, ignore this message', WARNING)
        else:
            logger.console('Now creating an SSD model for %d classes + bkg class' % model_num_positive_classes)

        ###########################################
        # Setting up Pytorch and building network #
        ###########################################
        logger.console('Setup pytorch...')
        self.cuda_on = setup_pytorch03(args=args, logger=logger, suppress_warnings=True)
        if self.cuda_on:
            logger.console('Getting device count:')
            device_count = torch.cuda.device_count()
            msg_device = '... There are %d devices ...' % device_count if device_count > 1 else '... Found 1 device ...'
            logger.console(msg_device)

        logger.console('Constructing the SSD net...')
        variant = 512 if max(self.target_image_size) >= 400 else 300
        if args.feature_extraction_type == 'vgg16':
            freeze_n = 21
            self.ssd_net = build_ssd_with_vgg(
                cfg=config_params,
                phase='train',
                variant=variant,
                size=self.target_image_size,
                num_classes=config_params['num_classes'],
                vgg_type='vgg16',
                dropout=args.dropout
            )
        elif args.feature_extraction_type == 'vgg19':
            freeze_n = 25
            self.ssd_net = build_ssd_with_vgg(
                cfg=config_params,
                phase='train',
                variant=variant,
                size=self.target_image_size,
                num_classes=config_params['num_classes'],
                vgg_type='vgg19'
            )
        elif args.feature_extraction_type == 'squeezenet1_0':
            freeze_n = 10
            self.ssd_net = build_ssd_with_squeezenet(
                cfg=config_params,
                phase='train',
                variant=variant,
                size=self.target_image_size,
                num_classes=config_params['num_classes'],
                version=1.0
            )
        elif args.feature_extraction_type == 'squeezenet1_1':
            freeze_n = 7
            self.ssd_net = build_ssd_with_squeezenet(
                cfg=config_params,
                phase='train',
                variant=variant,
                size=self.target_image_size,
                num_classes=config_params['num_classes'],
                version=1.1)
        elif args.feature_extraction_type == 'resnet50':
            raise ValueError('"resnet50" is not supported at the moment')
            # freeze_n = 16
            # self.ssd_net = build_ssd_with_resnet(cfg=config_params, phase='train',
            #                                 variant=variant, size=target_image_size,
            #                                 num_classes=config_params['num_classes'], resnet_type='resnet50')
        else:
            raise ValueError('Unsupported feature extraction type {}'.format(args.feature_extraction_type))

        self.utils = utils(
            softmax=torch.nn.Softmax(dim=-1),
            decode_boxes=Detect(
                num_classes=config_params['num_classes'],
                bkg_label=0,
                top_k=config_params.get('top_k', 200),
                conf_thresh=config_params.get('conf_thresh', 0.01),
                nms_thresh=config_params['nms_thresh'],
                cfg=config_params
            ))

        #############################
        # Configure loss and solver #
        #############################
        logger.console('... Done. Now configuring optimizer')
        if not args.end2end_train:
            # we freeze the feature extractor
            freeze_first_n_layers(self.ssd_net.features, freeze_n)

        ###################################
        # Load input model weights to net #
        ###################################
        weights_file = self.input_model.get_weights()
        init_weight_function = partial(default_weight_init_for_module, method='xavier', logger=logger)
        if weights_file is None:
            logger.console('Initializing weights...')
            self.ssd_net.apply(init_weight_function)
        else:
            logger.console('Loading weights from: {}'.format(weights_file))
            self.ssd_net.forgiving_load(weights_file, logger=logger)

        # wrap to enable training on multiple GPUs
        self.net = torch.nn.DataParallel(self.ssd_net) if self.cuda_on else self.ssd_net
        self.net_design = config_params

    def _train(self, start_iter, stop_iter):
        logger = self.logger
        args = self.args
        logger.console(">>> Training ")

        # Debug timers
        step_timer = Timer()
        preprocess_timer = Timer()
        convert_to_torch = Timer()
        total_timer = Timer()

        self.id_to_label_mapping = {v: k for k, v in self.output_model_labels.items()}
        draw_boxes_with_mapping = DrawBoxesAndLabels(labels_mapping=self.id_to_label_mapping, logger=logger)

        assert isinstance(self.task, Task)
        logger.console('... Train commence - getting iterator.')
        train_iterator = self.train_dataview.get_iterator()

        params = filter(lambda p: p.requires_grad, self.net.parameters())
        optimizer = torch.optim.SGD(
            params,
            lr=self.net_design['lr_list'][0],
            momentum=args.momentum,
            weight_decay=args.weight_decay
        )

        lr_scheduler = CyclicLRScheduler(
            optimizer=optimizer,
            upper_bound_steps=self.net_design['lr_steps'],
            lr_list=self.net_design['lr_list']
        )

        criterion = DragonLoss(
            num_classes=self.net_design['num_classes'],
            prior_for_matching=True,
            encode_target=False,
            overlap_thresh=self.net_design['multi_box_loss_overlap_thresh'],
            sampled_bkg=args.num_sampled_bkg,
            min_hard_bkg=args.min_hard_bkg,
            neg_mining=True,
            bkg_label=self.net_design['background_label'],
            neg_pos=self.net_design['negative_positive_ratio'],
            neg_overlap=0.5,
            variance=self.net_design['variance'],
            use_gpu=self.cuda_on)

        pipe = DataPipe(
            train_iterator,
            num_workers=args.num_workers,
            frame_cls_kwargs=dict(
                target_width_height=self.target_image_size,
                resize_strategy=ResizeStrategy.BIGGER_KEEP_ASPECT_RATIO
            )
        )
        pipe_iterator = pipe.get_iterator()

        self.net.train()

        # loss counters
        cumulative_loc_loss_reported = 0
        cumulative_conf_loss_reported = 0

        batcher_func = make_ssd_batch
        batcher = partial(batcher_func, target_image_size_wh=self.target_image_size)

        iteration = start_iter

        logger.console('Got it. Training Start!')
        timing_histogram = None

        pre_batch_collect = []

        preprocess_timer.tic()
        total_timer.tic()
        for cnt, image_frame in enumerate(pipe_iterator):
            if iteration > stop_iter:
                break

            # collect the frames into a batch (i.e. a list of tuples)
            pre_batch_collect.append(image_frame)

            if len(pre_batch_collect) != args.batch_size:
                continue

            image_batch = pre_batch_collect
            pre_batch_collect = []
            batch = batcher(image_batch)

            preprocess_timer.toc()
            convert_to_torch.tic()

            batch_input = [torch.from_numpy(inp) for inp in batch.input]
            targets = [torch.from_numpy(t) for t in batch.targets]
            if len(batch.targets) != len(batch_input[0]):
                logger.console('ERROR - problem with batcher? targets should be the same size of input', level=ERROR)
                targets = [np.array([0.0001, 0.0001, 0.0001, 0.0001, 0]).reshape(1, 5) * batch.input[0].size]
                targets = [torch.from_numpy(t) for t in targets]

            batch_input = [Variable(inp.cuda()) if self.cuda_on else Variable(inp) for inp in batch_input]
            targets = [Variable(ann.cuda(), volatile=True) if self.cuda_on else Variable(ann, volatile=True) for ann in
                       targets]

            convert_to_torch.toc()
            step_timer.tic()

            # forward
            out = self.net(*batch_input)

            optimizer.zero_grad()
            loss_l, loss_c = criterion(out, targets)
            loss = loss_l + loss_c

            # back-propagation
            loss.backward()

            # optimization
            lr_scheduler.step()
            optimizer.step()

            # we adopt a convention where the iteration count is "how many steps taken"
            iteration += 1

            step_timer.toc()

            # send statistics
            if iteration and iteration % args.report_iterations == 0:
                this_iter_loss = loss.item()
                this_iter_loss_l = loss_l.item()
                this_iter_loss_c = loss_c.item()
                if self.debug_report_loss_internals:
                    logger.console('Iteration %-6d [DRAGON-LOSS] POS/NEG: %4d/%-4d Denominator: %d' %
                                   (iteration, criterion.last_pos_num, criterion.last_neg_num, criterion.norm_coeff))

                # grab loss for this iteration
                if np.isfinite(this_iter_loss_l):  # todo - pytorch0.4 deprecated the [0] index for 0-dim tensor
                    cumulative_loc_loss_reported += this_iter_loss_l
                else:
                    logger.console('NOTE: Localization loss was %f in iteration %d' % (this_iter_loss_l, iteration))
                if np.isfinite(this_iter_loss_c):
                    cumulative_conf_loss_reported += this_iter_loss_c
                else:
                    logger.console('NOTE: Classification loss was %f in iteration %d' % (this_iter_loss_c, iteration))

                # Scalar Reporting
                logger.report_scalar(
                    title='Localization Loss',
                    series='iter. mean',
                    iteration=iteration,
                    value=cumulative_loc_loss_reported / float(iteration / args.report_iterations)
                )
                logger.report_scalar(
                    title='Localization Loss',
                    series='current',
                    iteration=iteration,
                    value=this_iter_loss_l
                )
                logger.report_scalar(
                    title='Classification Loss',
                    series='iter. mean',
                    iteration=iteration,
                    value=cumulative_conf_loss_reported / float(iteration / args.report_iterations)
                )
                logger.report_scalar(
                    title='Classification Loss',
                    series='current',
                    iteration=iteration,
                    value=this_iter_loss_c
                )
                for idx, param_group in enumerate(optimizer.param_groups):
                    logger.report_scalar(
                        title='Learning-Rate',
                        series='Param Group %d' % idx,
                        iteration=iteration,
                        value=param_group['lr']
                    )

                # Note: iteration time reported as rates
                logger.report_scalar(
                    title='Rates[1/sec]',
                    series='images',
                    iteration=iteration,
                    value=(batch.size / total_timer.average_time)
                )
                logger.report_scalar(
                    title='Rates[1/sec]',
                    series='iterations',
                    iteration=iteration,
                    value=(1 / total_timer.average_time)
                )
                if self.timers_benchmark_windows > 0:
                    logger.report_scalar(
                        title='Window Average (size %d)' % self.timers_benchmark_windows,
                        series='Iter time',
                        iteration=iteration,
                        value=total_timer.average_time
                    )

                n_report = 7
                # draw histogram of timing last n*2 iterations
                # overhead time:
                overhead = total_timer.average_time - (
                        step_timer.average_time + convert_to_torch.average_time + preprocess_timer.average_time)
                this_histogram = np.atleast_2d(np.around(np.array(
                    [preprocess_timer.average_time,
                     convert_to_torch.average_time,
                     step_timer.average_time,
                     overhead]) / batch.size, decimals=3)).T
                timing_histogram = np.hstack(
                    (timing_histogram[:, -(n_report - 1):],
                     this_histogram)) if timing_histogram is not None else this_histogram
                histogram_labels = ['IO & pre', 'convert', 'step', 'overhead']
                if timing_histogram.shape[-1] >= n_report:
                    logger.report_vector(
                        title='Average Timing - last %d reports [sec/image]' % n_report,
                        series='average',
                        iteration=0,
                        values=timing_histogram,
                        labels=histogram_labels
                    )
                    timing_histogram = None

                self.logger.flush()

                # Image Reporting
                if (iteration // self.args.report_iterations) % args.report_images_every_n_reports == 0:
                    t_images = time.time()
                    pred_probs = self.utils.softmax(out[1])
                    detections = self.decode_boxes(out[0], pred_probs, out[2])
                    detections = detections.clamp(0., 1.)
                    detections = detections.data.cpu().numpy()
                    all_batch_boxes, all_batch_scores = \
                        ssd_output_to_allegro_format(
                            numpy_ssd_output=detections,
                            conf_thresh=self.net_design.get('train_conf_thresh_debug_images', 0.5),
                            img_width=self.target_image_size.w,
                            img_height=self.target_image_size.h
                        )

                    # PyTorch images are (3, h, w), for cv2 we need:
                    #  (h, w, 3) transpose + RGB2BGR cycling
                    img_to_draw = []
                    for input_type in batch.input:
                        if input_type.shape[1] == 4:
                            first_three = input_type[:, :3, :, :]
                            first_three = first_three.transpose(0, 2, 3, 1)
                            img_to_draw.append(first_three)

                            last = input_type[:, 3:, :, :]
                            last = np.repeat(last, 3, axis=1)
                            last = last.transpose(0, 2, 3, 1)
                            img_to_draw.append(last)
                        elif input_type.shape[1] == 3:
                            np_images = input_type.transpose(0, 2, 3, 1)
                            img_to_draw.append(np_images)
                        else:
                            raise ValueError('Only 3 or 4 channels per input are supported ')

                    data_name = ['Images']
                    if len(data_name) < len(img_to_draw):
                        logger.console('Too many inputs, not designed for such amount, using default names',
                                       level=ERROR)
                        data_name.extend([''] * len(img_to_draw))
                    for stage_id, np_images in enumerate(img_to_draw):
                        draws = draw_boxes_with_mapping(
                            images=np_images,
                            pred_scores=all_batch_scores,
                            pred_boxes=all_batch_boxes,
                            gt_boxes=batch.ground_truth)
                        for idx, img in enumerate(draws):
                            logger.report_image_and_upload(
                                title='%s' % data_name[stage_id],
                                series='img_%d' % idx,
                                iteration=iteration,
                                matrix=img.astype(np.uint8))

                    t_images = time.time() - t_images
                    logger.console('Time to create debug images: %.4f (sec)' % t_images)
                    logger.flush()

                # General log printouts
                msg = 'Iteration %-6d : t_iter %.3f (sec) | Loss %.4f' % \
                      (iteration, total_timer.average_time, this_iter_loss)
                logger.console(msg)
                if self.timers_benchmark_windows > 0 and (
                        iteration // args.report_iterations) % self.timers_benchmark_windows == 0:
                    logger.console('Resetting timer averages at iteration %d' % iteration)
                    step_timer.reset_average()
                    preprocess_timer.reset_average()
                    convert_to_torch.reset_average()
                    total_timer.reset_average()
            total_timer.toc()

            # store snapshot
            if iteration != 0 and iteration % args.save_iterations == 0:
                logger.console('Saving state, iter: %d' % iteration)
                upload_model_snapshot(net=self.ssd_net, model=self.output_model, iteration=iteration)
                if self.test_while_train:
                    self._test(at_iteration=iteration)

            # Start timimg now so we do not add the test time to the average total time per step
            total_timer.tic()
            preprocess_timer.tic()

        # we are done, upload the last snapshot
        upload_model_snapshot(net=self.ssd_net, model=self.output_model, iteration=iteration)

        if self.test_while_train:
            self._test(at_iteration=iteration)

        logger.console('All done! Have a nice day!')
        self.task.mark_stopped()

    def _test(self, at_iteration=0):
        args = self.args
        logger = self.logger
        logger.console(">>> Training at iteration %d" % at_iteration)

        # hardcoded parameters:
        precision_recall_conf_list = [0.001, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 0.99, 0.999]
        min_precision_for_integrated_metric = 0.7

        if args.test_size <= 0 or not self.test_dataview:
            logger.console('Skipping test, test_size=0')
            return

        label_to_id_mapping_for_test = self.test_dataview.get_labels()
        id_to_label_mapping_for_test = {v: k for k, v in label_to_id_mapping_for_test.items()}
        score_thresh = self.net_design.get('test_detection_conf_thresh', 0.5)
        draw_boxes_with_mapping_for_test = DrawBoxesAndLabels(
            labels_mapping=id_to_label_mapping_for_test,
            logger=logger,
            score_threshold=score_thresh
        )

        # Set net to eval mode for testing
        self.net.eval()

        logger.console('%s' % str(80 * '-'))
        t_test = time.time()
        iou = [i for i in self.net_design['test_detection_iou_thresh']]

        batcher_func = make_ssd_batch
        batcher = partial(batcher_func, target_image_size_wh=self.target_image_size)

        iterator = self.test_dataview.get_iterator()

        test_pipe = DataPipe(
            iterator,
            num_workers=args.num_workers,
            frame_cls_kwargs=dict(
                target_width_height=self.target_image_size,
                resize_strategy=ResizeStrategy.BIGGER_KEEP_ASPECT_RATIO
            )
        )
        test_pipe_iterator = test_pipe.get_iterator()
        logger.console('Testing currently loaded weights...')

        box_history = BoxesHistory()

        pre_batch_collect = []
        last_key_frame = False
        all_batch_boxes = None
        all_batch_scores = None
        flt_mean = 0
        report_iter = 0
        for test_iter, image_frame in enumerate(test_pipe_iterator):
            if test_iter > args.test_size:
                break

            pre_batch_collect.append(image_frame)
            if len(pre_batch_collect) != args.batch_size:
                continue

            # collect the frames into a batch (i.e. a list of tuples)
            image_batch = pre_batch_collect
            pre_batch_collect = []
            # use the batch and zero it at the end

            # test batch
            batch = batcher(image_batch)
            batch_input = [torch.from_numpy(inp) for inp in batch.input]
            batch_input = [Variable(inp.cuda()) if self.cuda_on else Variable(inp) for inp in batch_input]

            # forward
            out = self.net(*batch_input)
            pred_probs = self.utils.softmax(out[1])
            detections = self.utils.decode_boxes(out[0], pred_probs, out[2])
            detections = detections.clamp(0., 1.)
            detections = detections.data.cpu().numpy()
            all_batch_boxes, all_batch_scores = \
                ssd_output_to_allegro_format(
                    numpy_ssd_output=detections,
                    conf_thresh=min(precision_recall_conf_list),
                    img_width=self.target_image_size.w,
                    img_height=self.target_image_size.h
                )

            # Make sure we have hard rois
            all_ground_truth = tuple(bb for bb in (batch.ground_truth, batch.hard_ground_truth) if bb.size)
            if all_ground_truth:
                all_ground_truth = np.vstack(all_ground_truth)
                box_history.save_frame_data(
                    pred_boxes=all_batch_boxes,
                    pred_scores=all_batch_scores,
                    gt_boxes=all_ground_truth
                )
            else:
                all_ground_truth = None

            report_iter += 1
            if report_iter and report_iter % args.report_iterations == 0:
                msg = 'Test iteration %-6d' % report_iter
                logger.console(msg)
            if report_iter and report_iter % (args.report_iterations * args.report_images_every_n_reports) == 0:
                t_images = time.time()
                data_name = ['Images']
                img_to_draw = []
                for input_type in batch.input:
                    if input_type.shape[1] == 4:
                        first_three = input_type[:, :3, :, :]
                        first_three = first_three.transpose(0, 2, 3, 1)
                        img_to_draw.append(first_three)

                        last = input_type[:, 3:, :, :]
                        last = np.repeat(last, 3, axis=1)
                        last = last.transpose(0, 2, 3, 1)
                        img_to_draw.append(last)
                    elif input_type.shape[1] == 3:
                        np_images = input_type.transpose(0, 2, 3, 1)
                        img_to_draw.append(np_images)
                    else:
                        raise ValueError('Only 3,4 channels are supported')

                if len(data_name) < len(batch.input):
                    logger.console('Too many inputs, not designed for such amount, using default names', level=ERROR)
                    data_name.extend([''] * len(batch.input))
                for stage_id, np_images in enumerate(img_to_draw):
                    draws = draw_boxes_with_mapping_for_test(
                        images=np_images,
                        pred_scores=all_batch_scores,
                        pred_boxes=all_batch_boxes,
                        gt_boxes=all_ground_truth,
                        gt_boxes_labels=batch.ground_truth_labels,
                        gt_boxes_hard=batch.hard_ground_truth_boxes,
                        gt_boxes_hard_labels=batch.hard_ground_truth_labels
                    )

                    draws = [cv2.putText(im, 'TEST', (10, 30), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255))
                             for im in draws]

                    for idx, img in enumerate(draws):
                        logger.report_image_and_upload(
                            title='Test - %s' % data_name[stage_id],
                            series='test_img_%d' % idx,
                            iteration=at_iteration,
                            matrix=img.astype(np.uint8)
                        )

                t_images = time.time() - t_images
                logger.console('Iter %d Time to create test images: %.4f (sec)' % (report_iter, t_images))

        # Report results
        id_to_label = {v: k for k, v in label_to_id_mapping_for_test.items() if v > 0}
        logger.console('Calculating precision and creating report...')

        # epsilon for numerical methods:
        epsilon = np.finfo(float).eps

        for iou_thresh in iou:
            pr_curves_per_label = box_history.get_precision_recall_curve(
                iou_threshold=iou_thresh,
                conf_thresholds=precision_recall_conf_list,
                nms_threshold=self.net_design.get('nms_thres_for_metrics', 0.65))

            all_ap = []
            all_neap = []
            simple_title = '{} | IOU=%3.2f' % iou_thresh
            for id in pr_curves_per_label:
                pr_curve = pr_curves_per_label[id]
                label_str = id_to_label.get(id, 'Label %d' % id)
                ap = average_precision_from_curve(pr_curve)
                ap_with_min_prec = average_precision_from_curve(pr_curve,
                                                                min_precision=min_precision_for_integrated_metric)
                prec_rec_title = '[{label_str}] Precision-Recall @IOU:{iou_thresh}'
                conf_f1_title = '[{label_str}] Conf-F1-score @IOU:{iou_thresh}'
                min_prec = min_precision_for_integrated_metric
                series_title = 'it: {at_iteration} ap: {ap:3.2f} nEmAP_{min_prec:3.2f}: {ap_with_min_prec:3.2f}'

                prec_recall_graph = [(recall, precision) for _, precision, recall in pr_curve.raw_curve]
                conf_fscore_graph = [(conf, ((2 * recall * precision) / (epsilon + recall + precision)))
                                     for conf, precision, recall in pr_curve.raw_curve]
                top_fscore = max([x[1] for x in conf_fscore_graph])
                logger.console('AP=%.2f AP_with_min_prec=%.2f' % (ap, ap_with_min_prec))
                logger.report_scatter2d(
                    labels=['conf_thr=%3.2f' % x[0] for x in conf_fscore_graph],
                    title=prec_rec_title.format(**locals()),
                    series=series_title.format(**locals()),
                    iteration=-1,
                    xaxis='Recall',
                    yaxis='Precision',
                    scatter=prec_recall_graph)

                logger.report_scatter2d(
                    labels=['Recall=%3.2f, Precision=%3.2f' % (x[0], x[1]) for x in prec_recall_graph],
                    title=conf_f1_title.format(**locals()),
                    series=series_title.format(**locals()),
                    iteration=-1,
                    xaxis='Conf. Threshold',
                    yaxis='F1-Score',
                    scatter=conf_fscore_graph)

                logger.report_scalar(
                    title=simple_title.format('Top F1-score'),
                    series='%s' % label_str,
                    iteration=at_iteration,
                    value=top_fscore)
                logger.report_scalar(
                    title=simple_title.format('normalized Effective AP_%3.2f' % min_prec),
                    series='%s' % label_str,
                    iteration=at_iteration,
                    value=ap_with_min_prec)
                all_neap.append(ap_with_min_prec)
                logger.report_scalar(
                    title=simple_title.format('Average Precision'),
                    series='%s' % label_str,
                    iteration=at_iteration,
                    value=ap)
                all_ap.append(ap)

                logger.console(simple_title.format('Precision-Recall'))
                logger.console(prec_recall_graph)
                logger.console('-------------------------------------------------')
                logger.console(simple_title.format('Conf-F1-score'))
                logger.console(conf_fscore_graph)
                logger.console('-------------------------------------------------')

            logger.report_scalar(
                title=simple_title.format('normalized Effective AP_%3.2f' % min_precision_for_integrated_metric),
                series='mnEAP_%3.2f' % min_precision_for_integrated_metric,
                iteration=at_iteration,
                value=np.mean(all_neap))
            logger.report_scalar(
                title=simple_title.format('Average Precision'),
                series='mAP',
                iteration=at_iteration,
                value=np.mean(all_ap))

            logger.flush()

        logger.console('Finished testing snapshot!')
        logger.console('%s' % str(80 * '-'))
        msg = 'Iteration %-6d : test time %.3f (sec)' % (at_iteration, time.time() - t_test)
        logger.console(msg)
        logger.flush()

        # Set the net to training mode when done testing
        if not args.only_test:
            self.net.train()

    @staticmethod
    def _get_parser(input_parser=None):

        parser = input_parser or ArgumentParser(description='Single Shot MultiBox Detector Training With Pytorch')

        # (0) Basic config
        parser.add_argument('--batch-size', default=6, type=int, help='Batch Size')
        parser.add_argument('--max-iterations', default=170000, type=int, help='Max number of iterations for train')
        parser.add_argument('--save-iterations', default=5000, type=int, help='Save model every K iters')
        parser.add_argument('--report-iterations', default=50, type=int, help='Report Iterations')
        parser.add_argument('--report-images-every-n-reports', default=3, type=int, help='Images report frequency')
        parser.add_argument('--num-workers', default=32, type=int,
                            help='Number of workers used for parallel data loading')
        parser.add_argument('--upload-destination', default='s3://allegro-examples', type=str,
                            help='Destination to upload debug images and models')

        # (1) Model control
        parser.add_argument('--feature-extraction-type', default='vgg16', type=str, help='Feature extraction network')
        parser.add_argument('--dropout', default=0.1, type=bound_number_type(minimum=0, maximum=1, dtype=float),
                            help='Feature extraction network')

        # (2) Testing control
        parser.add_argument('--test-size', default=500, type=int, help='Number of iterations for test')
        parser.add_argument('--test-while-train', default=1, type=int, help='Run a test before training')
        parser.add_argument('--test-before-train', default=1, type=int, help='Run a test before training')
        parser.add_argument('--only-test', default=0, type=int, help='Only run the test code')
        parser.add_argument('--test-dataview-id', default='', type=str,
                            help='Dataview in the task project that will be used as test set')

        # (3) pytorch specific
        parser.add_argument('--cuda', default=1, type=int, help='Use CUDA to train model')

        # (3.1) dragon loss
        parser.add_argument('--num-sampled-bkg', type=int, default=300,
                            help='How many bkg samples to use with dragon loss')
        parser.add_argument('--min-hard-bkg', type=int, default=300,
                            help='Min number of hard bkg (if no FG is present)')

        # (3.2) solver control (This task uses SGD)
        parser.add_argument('--end2end-train', default=0, type=int, help='Train full net architecture')
        parser.add_argument('--momentum', default=0.9, type=float, help='SGD momentum')
        parser.add_argument('--weight-decay', default=0.0005, type=float, help='Weight decay coefficient')

        return parser


if __name__ == '__main__':

    project_name = 'pytorch ssd'
    task_name = 'Train SSD default example'

    dv = DataView()
    dv.add_query(
        dataset_name='COCO - Common Objects in Context',
        version_name='Train2017 version',
        roi_query='person'
    )
    dv.add_query(
        dataset_name='PASCAL Visual Object Classes',
        version_name='Train2012 version',
        roi_query='person'
    )

    # Any custom augmentations used must be registered as custom augmentations
    # ImageFrame.register_custom_augmentation(operation_name='AugmentationZoomInOut',
    #                                         augmentation_class=CustomAugmentationZoom)

    # Add Augmentation to train dataview
    aug_aff = Augmentation.Affine
    # train_dataview.add_augmentation_custom(operations=['AugmentationZoomInOut'], strength=1.67)
    dv.add_augmentation_affine(
        strength=1.0,
        operations=(aug_aff.bypass, aug_aff.reflect_horizontal)
    )
    dv.add_augmentation_affine(
        strength=1.5,
        operations=(aug_aff.bypass, aug_aff.rotate, aug_aff.shear, aug_aff.scale)
    )
    dv.add_augmentation_pixel(strength=1.5)
    dv.add_mapping_rule(
        dataset_name='PASCAL Visual Object Classes',
        version_name='Train2012 version',
        from_labels=['person', 'difficult'],
        to_label='ignore'
    )

    label_mapping = {'hard': -2, 'ignore': -1, 'background': 0, 'person': 1}

    args_override = dict(test_dataview_id="3a2bb3b0e0eb47f2b8d67554435d4bcc")

    experiment = SSDExperiment(
        project_name=project_name,
        task_name=task_name,
        train_dataview=dv,
        label_mapping=label_mapping,
        args_override=args_override
    ).run()
